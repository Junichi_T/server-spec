require 'spec_helper'
require 'Pony'

def user_command(user, cmd)
	  command("su -l #{user} -c '#{cmd}'")
end

services = [
	'apache2',
	'uchiwa',
	'ssh',
	'dovecot'
]

services.each do |s|
	describe service(s), :if => os[:family] == 'debian' do
		it { should be_enabled }
		it { should be_running }
	end
end

describe package('postfix') do
	  it { should be_installed }
end

describe service('postfix') do
	  it { should be_enabled   }
	    it { should be_running   }
end

describe port(25) do
	  it { should be_listening }
end

describe "Mail test" do
		stdout = system("ruby /tmp/pony_test.rb")
  	it 'Mail send succeeded' do
    	expect(stdout).to eq true
  	end
end

describe "Smtp" do
      Pony.options = {
              :to => 'j.tsurukawa@gmail.com',
              :from => 'tsurukawa@phil.flet.keio.ac.jp',
              :subject => 'Test from Serverspec',
              :body => 'Test from Serverspec',
              :via => :smtp,
              :via_options => {
                      :port => '25',
                      :enable_starttls_auto => false,
											:user_name => ENV['SENDGRID_USERNAME'],
											:password => ENV['SENDGRID_PASSWORD'],
											:authentication => :plain,
                      :domain => 'phil.flet.keio.ac.jp'
              }
      }

      it "should allow sending a mail" do
              expect { Pony.mail({}) }.to be_true
      end

      # it "should disallow sending mail on wrong password" do
      #         old_options = Pony.options
      #         Pony.options[:via_options][:password] = 'falsepass'
      #         expect { Pony.mail({}) }.to raise_error
      #         Pony.options = old_options
      # end
end
